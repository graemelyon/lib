%
% annotateRel
% Place an annotation relative to the top-left of the axis
%
% Graeme Lyon
% Nov 2015
%
function h = annotateRel(gca, str, xOff, yOff)

axDim = get(gca,'Position');

xOff = xOff*(axDim(3));
yOff = yOff*(axDim(4));
h = annotation('textbox',[axDim(1)+xOff (axDim(2)+axDim(4))-yOff 0 0], ...
	'String',str,'FitBoxToText','on','fontsize',9,'BackgroundColor',[0.97 0.97 0.97]);

end