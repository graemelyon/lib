%fvvar - fast vector variance estimate.  Assumes that input
%is a vector, and always normalized by N-1.  This is much
%faster then mathworks version which has a lot of overhead
%to deal with the mathworks versions ability to handle matricies.
function y=fvvar(x)
    n=length(x);
    xbar = fvmean(x);
    y = sum((x-xbar).^2)./(n-1);
    