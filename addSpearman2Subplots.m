function h=addSpearman2Subplots

axesObjs = get(gcf, 'Children');  %axes handles
dataObjs = get(axesObjs, 'Children'); %handles to low-level graphics objects in axes

for i=1:length(dataObjs)
    
    if length(dataObjs)==1
        xdata = get(dataObjs(i), 'XData');  %data from low-level grahics objects
        ydata = get(dataObjs(i), 'YData');
    else
        xdata = get(dataObjs{i}, 'XData');  %data from low-level grahics objects
        ydata = get(dataObjs{i}, 'YData');
    end
    if iscell(xdata)
        x=[];
        for c=1:numel(xdata)
            x = [x; xdata{c}(:) ydata{c}(:)];
        end
    else
        x = [xdata(:),ydata(:)];
    end
    
    if ~isempty(x)
        rho=spearman(x);
        str = sprintf('\\rho=%0.2f',rho);
        annotateRel(axesObjs(i), str, 0.01, 0.01);
    end
    
end