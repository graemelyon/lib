function stat = getStats(ref, h)
excl = (isnan(ref) | isnan(h));
ref(excl)=[]; h(excl)=[];


if size(ref,1)>1
    err = sum(ref-h,2);
else
    err = ref-h;
end
    stat.bias = mean(err);
stat.std = std(err);
stat.rmsd = sqrt(stat.bias^2 + stat.std^2);
stat.median = median(err);
stat.min = min(err);
stat.max = max(err);
stat.pct5 = prctile(err,5);
stat.pct95 = prctile(err,95);
% stat.r = pearson(ref, h);
if length(ref(:)) > 2
    stat.spearman = spearman([ref(:) h(:)]);
    p = polyfit(ref, h,1);
    f = polyval(p,ref);
    [stat.r2,~] = rsquare(h,f);
end