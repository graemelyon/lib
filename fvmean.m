%fvmean: fast vector mean
%this version is much faster then the mathworks version
%of mean.  It assumes that the input is a vector!!!
%mathworks version spends more time figuring out
%the dimensions of the input then it does to actual calculate
%the mean.  
function y=fvmean(x)
    %sum is a built-in (C-coded) function, so its super fast
    y=sum(x)./length(x);
end

