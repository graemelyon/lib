%fvstd: fast vector standard deviation.  See 
%Core\fvvar.m function for details.
function y=fvstd(x)
    y=sqrt(fvvar(x));

