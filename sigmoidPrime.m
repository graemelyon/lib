function g = sigmoidPrime(z)
s = sigmoid(z);
g = s.*(1-s);
end