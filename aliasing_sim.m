clf; clear; hold on;
fs = 44.1e3;

f = [20.5:0.05:22.5]*1e3;
f = [f fliplr(f)];

for i = 1:length(f)
    fa = fs.*[1] - f(i);
    for j=1:length(fa)
        h(1)=plot(i, abs(fa(j)), 'sb');
    end
end
h(2)=plot(f, 'or'); grid; axis([-inf inf 19e3 25e3]);
ylabel('Frequency (Hz)');
legend(h, 'Alias','Signal');