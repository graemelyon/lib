function [Spectra,f] = DoFFT(data, fs, scale)
% Calculates an FFT on 'data' at sample rate fs
% and scales output to either power/or magnitude
% ovr increases Nfft bins to improve freq location from smoother peaks

L = length(data);                       %Length of data
NFFT = 2^(nextpow2(L));                 %No bins, closest pow2 + oversample
f = (fs/2)*linspace(0,1,NFFT/2+1);      %Frequency out vector

%FFT
Y = fft(data,NFFT) / sqrt(NFFT);        %Perform FTT (Energy = Y^2/nfft)

if strcmp(scale, 'pow')
    Spectra = Y.*conj(Y);               %Power
elseif strcmp(scale, 'mag')
    Spectra = abs(Y);                   %Magnitude
end

Spectra = Spectra(1:NFFT/2+1);              %single sided spectrum  DC,f1,f2,f3,f4...f4,f3,f2,f1
Spectra(2:end) = Spectra(2:end)*sqrt(2);    %we've lost half the spectrum so increase the energy except DC

end
