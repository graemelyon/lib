function rho = spearman(x)
% SPEARMAN Rank order correlation matrix 
%
% S = SPEARMAN(X) For data matrix X. Spearman Rank Correlation Coefficient 
% is a nonparametric (distribution-free) rank statistic proposed by 
% Spearman in 1904 as a measure of the strength of the associations 
% between two variables. The Spearman rank correlation coefficient is a 
% measure of monotone association that is used when the distribution of 
% the data make Pearson's correlation coefficient misleading
%
% See Also : PEARSON, KENDALL, SIGNRANK, tiedrank_, RANKSUM

% Author: Tim Gebbie 

% $Revision: 1.1 $ $Date: 2004-10-18 13:05:07+02 $ $Author: tgebbie $

% GL Nov 2015
% NOTE: inputs should be of the form [x(:), y(:)]

% find the size of the data
[n,m] = size(x);
% find the spearman correlation
for i = 1:m,
    % find the statistical ranks
    ri = tiedrank_(x(:,i),1);
    for j = 1:m,
        % find the statistical ranks
        rj = tiedrank_(x(:,j),1);
        % find the difference between the statistical ranks
        d = ri(:) - rj(:);
        % find the spearman co-efficient
        s(i,j) = 1 - (6 /( n * (n^2 -1))) * (d' * d);
    end; % j
end; % i

rho = s(1,2);

