function r = pearson(x,y)
stdy = fvstd(y);
stdx = fvstd(x);
xbar = fvmean(x);
ybar = fvmean(y);
x = (x-xbar)./stdx;
y = (y-ybar)./stdy;
n = length(x)-1;
r = (1/n)*sum( (x).*(y) );