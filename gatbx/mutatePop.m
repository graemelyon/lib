%
% recombineIntermediate
%
% Medtronic
% Graeme Lyon
% Jan 2015
%
% Based on GA work from Sheffield Univsersity (http://codem.group.shef.ac.uk/index.php/ga-toolbox)

function NewChrom = mutatePop(OldChrom, fieldLims, mutProb, mutShrink)
% Parameters are mutated with probability MutR
% NewChrom = OldChrom (+ or -) * Range * MutShrink * Delta
% Range = 0.5 * (upperbound - lowerbound)
% Delta = Sum(Alpha_i * 2^-i) from 0 to ACCUR; Alpha_i = rand(ACCUR,1) < 1/ACCUR


% Identify the population size (Nind) and the number of variables (Nvar)
[Nind,Nvar] = size(OldChrom);


% Matrix with range values for every variable
Range = rep(0.5 * mutShrink *(fieldLims(2,:)-fieldLims(1,:)),[Nind 1]);

% zeros and ones for mutate or not this variable, together with Range
Range = Range .* (rand(Nind,Nvar) < mutProb);

% compute, if + or - sign
Range = Range .* (1 - 2 * (rand(Nind,Nvar) < 0.5));

% used for later computing, here only ones computed
ACCUR = 20;
Vect = 2 .^ (-(0:(ACCUR-1))');
Delta = (rand(Nind,ACCUR) < 1/ACCUR) * Vect;
Delta = rep(Delta, [1 Nvar]);

% Perform mutation
NewChrom = OldChrom + Range .* Delta;

% Enforce parameter boundaries
NewChrom = max(rep(fieldLims(1,:),[Nind 1]), NewChrom);
NewChrom = min(rep(fieldLims(2,:),[Nind 1]), NewChrom);


% End of function