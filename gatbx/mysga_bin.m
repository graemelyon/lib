% sga.m
%
% This script implements the Simple Genetic Algorithm described
% in the examples section of the GA Toolbox manual.
%
% Author:     Andrew Chipperfield
% History:    23-Mar-94     file created
%
% tested under MATLAB v6 by Alex Shenfield (22-Jan-03)

clc; clear; close all;

studies = {'JugBulb#2' };
[fSO2_all, spectra_all, rSO2_2wl_all] = load_data(studies); %[deep shall]


wts = load('C:\Users\Graeme.Lyon\Documents\workspace\invos\ALGDEV\GLAnalysis\Optimization\wts\tmpLin_wts_4.6_goodR.mat');
ip = [spectra_all(:,5:8) spectra_all(:,1:4) ones(size(spectra_all,1),1)];
rSO2_4wl_linear = ip*wts.weights;
% Set target
target = fSO2_all;% - rSO2_4wl_linear;



NIND = 40;              % Number of individuals per subpopulations
MAXGEN = 100e3;        % maximum Number of generations
GGAP = .9;             % Generation gap, how many new individuals are created
NVAR = 9;              % Number of variables
PRECI = 10;             % Precision of binary representation
XOV = 0.7;              % Crossover rate


% Build field descriptor
fieldD = [rep([PRECI],[1, NVAR]); ...
    rep([-500;500],[1, NVAR-1 ]) [50;80];...
    rep([1; 0; 1 ;1], [1, NVAR])];

% Initialise population
Chrom = crtbp(NIND, NVAR*PRECI);

% Reset counters
Best = NaN*ones(MAXGEN,1);	% best in current population
gen = 0;			% generational counter

% Evaluate initial population
ObjV = myobjfun1(bs2rv(Chrom, fieldD), spectra_all, target);

% Track best individual and display convergence
Best(gen+1) = min(ObjV);
mkr = {'b','marker','o','markersize',4};
plot(Best,mkr{:});xlabel('generation'); ylabel('f(x)');
text(0.5,0.95,['Best = ', num2str(Best(gen+1))],'Units','normalized');
drawnow;

% Generational loop
while gen < MAXGEN,
    
    % Assign fitness-value to entire population
    %%FitnV = ranking(ObjV);
    FitnV = (1 - (ObjV ./ (sum(ObjV))));
    
    % Select individuals for breeding
    SelCh = select('sus', Chrom, FitnV, GGAP);
    
    % Recombine selected individuals (crossover)
    SelCh = recombin('xovsp',SelCh, XOV);
    
    % Perform mutation on offspring
    %SelCh = mutate('mutbga', SelCh, fieldD);
    SelCh = mut(SelCh);
    
    % Evaluate offspring, call objective function
    ObjVSel = myobjfun1(bs2rv(SelCh, fieldD), spectra_all, target);
    
    % Reinsert offspring into current population
    [Chrom, ObjV]=reins(Chrom,SelCh,1,1,ObjV,ObjVSel);
    
    % Increment generational counter
    gen = gen+1;
    
    % Update display and record current best individual
    if rem(gen,50)==0
        Best(gen+1) = min(ObjV);
                
        idx = max(1,gen-2500):gen;
        plot(idx, Best(idx),mkr{:}); xlabel('generation'); ylabel('f(x)'); grid on;
        yl = get(gca,'ylim');
        ylim([yl(1)-1 yl(2)+1]);
        xlim([idx(1) idx(end)+25]);
        %plot([idx(1) idx(end)], [5 5],'r');
        h1=text(0.45,0.95,['RMSD = ', num2str(Best(gen+1),'%0.3f')],'Units','normalized');
        set(h1,'FontSize',13);
        drawnow;
    end
end

% Convert Chrom to real-values
Phen = bs2rv(Chrom(ObjV==min(ObjV)), FieldD);

% End of GA