%
% GATrain4wlEqn
%
% Graeme Lyon
% Jan 2015
%

clc; clear; close all;

%-------- Load Data -----------------------------------------------------------
studies = {'JugBulb#2'};
[fSO2_all, spectra_all, rSO2_2wl_all] = load_data(studies); %[deep shall]

% Set target
target = fSO2_all;


%-------- Parameters ----------------------------------------------------------
NPop = 50;          % Number of individuals (per subpop)
MAXGEN = 30e3;      % maximum Number of generations
GGAP = .90;         % Generation gap, how many new individuals are created
MUTRATE = 0.05;     % Mutation rate
MUTSHRINK = 1;      % Mutation shrinkage (range of the mutation per gen)


%-------- Initialise Population ------------------------------------------------
fieldLims = [...
    %m               r               c   e
    rep(-500,[1,4])  rep(0.5,[1,4])  40  0  ; ...
    rep( 500,[1,4])  rep(1.5,[1,4])  80  0.5; ...
];
Chrom = createPopulation(NPop, fieldLims);

% Evaluate initial population
ObjV = evaluateObjective(Chrom, spectra_all, target);

% Plot
fh=figure; xlabel('generation'); ylabel('Cost'); mkr = {'r', 'marker','o','markersize', 3};


%-------- Train ---------------------------------------------------------------
Best = NaN*ones(MAXGEN,1);
gen = 0;
while (gen < MAXGEN)
    
    % Simple relative fitness
    FitnV = (1 - (ObjV ./ (sum(ObjV))));
    if any(isnan(FitnV)), error('Fitness contains NaNs - something''s wrong...'); end
    
    % Select individuals for breeding
    SelCh = RouletteWheelSelection(FitnV, GGAP, Chrom);
    
    % Recombine selected individuals (crossover)
    SelCh = recombineIntermediate(SelCh);
    
    % Perform mutation on offspring
    SelCh = mutatePop(SelCh, fieldLims, MUTRATE, MUTSHRINK);   
    
    % Evaluate offspring, call objective function
    ObjVSel = evaluateObjective(SelCh, spectra_all, target);
    
    % Reinsert offspring into current population
    [Chrom, ObjV] = reinsertPop(Chrom,SelCh,1,1,ObjV,ObjVSel);
    
    gen = gen+1;
    Best(gen+1) = min(ObjV);
    
    % Update plot
    if rem(gen,100)==0
        t_show = 5000;
        idx = max(1,gen-t_show):gen;
        set(0,'CurrentFigure',fh);
        plot(idx, Best(idx),mkr{:}); xlabel('generation'); ylabel('Cost'); grid on;
        yl = get(gca,'ylim');
        ylim([yl(1)-0.5 yl(2)+0.5]);
        xlim([idx(1) idx(end)+25]);
        %plot([idx(1) idx(end)], [5 5],'r');
        idx = max(1,gen-1000):gen;
        h=text(0.45,0.95,sprintf('Cost = %0.4f', Best(gen+1)), 'Units','normalized');
        set(h,'FontSize',13);
        drawnow;     
    end
    % Save best weights every so often
    if rem(gen,1000)==0
        wts = Chrom(ObjV==min(ObjV),:);
        save('best_wts','wts');
        nop;
    end
    
end


%-------- Save weights --------------------------------------------------------
wts = Chrom(ObjV==min(ObjV),:);
save('best_wts','wts');


% eof