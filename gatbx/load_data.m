function [fSO2_all, spectra_all, rSO2_2wl_all] = load_data(studies)

load('BloodDrawData_2.mat');
traceID = unique(allDATA.fileid);
fSO2_all=[]; spectra_all=[]; rSO2_2wl_all=[];

for i=1:length(traceID)
    
    id = traceID(i);
    fileName = allDATA.fileName{id};
    ndx = find(allDATA.fileid==id);
    % Only select this study
    splt = strsplit(fileName,'_');
    if ~(ismember(splt(1),studies))
        continue;
    end
    % Collect calibrated signals for each trace in study
    fSO2 = allDATA.fsO2(ndx);
    if (all(isnan(fSO2)))
        fSO2 = allDATA.casmed(ndx);
    end
    if (fSO2==0), continue; end
    fSO2_all = [fSO2_all; fSO2];
    
    S724 = allDATA.fshall724(ndx); S770 = allDATA.fshall770(ndx); S812 = allDATA.fshall812(ndx); S850 = allDATA.fshall850(ndx);
    D724 = allDATA.fdeep724(ndx);  D770 = allDATA.fdeep770(ndx);  D812 = allDATA.fdeep812(ndx);  D850 = allDATA.fdeep850(ndx);
    spectra = [D724 D770 D812 D850 S724 S770 S812 S850];
    spectra_all = [spectra_all; spectra];
    
    
    % Calc 2wl rSO2
    m = 121.3545;
    epsilon = 0.099935;
    %P2_SENSOR
    rds = 0.81;
    c = 72.75599 - 1.99;
    rSO2_2wl = m*((D724-D812) - rds.*(S724-S812));
    if true %P2
        rSO2_2wl = rSO2_2wl.*(1+epsilon.*D812);
    end
    rSO2_2wl = rSO2_2wl + c;
    
    rSO2_2wl_all = [rSO2_2wl_all; rSO2_2wl];
    
end