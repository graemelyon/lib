% 
% createPopulation
%
% create N individual random sets of weights between the limits 
% specificied in fieldLims, [lower_1,...,lower_n; upper_1,...,upper_n;]
% 
%
% Medtronic
% Graeme Lyon
% Jan 2015
%
function Chrom = createPopulation(N, fieldLims)

Nvar= size(fieldLims,2);

Range = rep((fieldLims(2,:)-fieldLims(1,:)),[N 1]);
Lower = rep(fieldLims(1,:), [N 1]);
Chrom = rand(N,Nvar) .* Range + Lower;
