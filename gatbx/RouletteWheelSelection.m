
function NewChrom = RouletteWheelSelection(FitnV, GGAP, Chrom)

% Identify the population size (Nind)
[Nind,~] = size(FitnV);

Nsel = max(floor(Nind*GGAP+.5),2);

% Perform Stochastic Sampling with Replacement
cumfit  = cumsum(FitnV);
trials = cumfit(Nind) .* rand(Nsel, 1);
Mf = cumfit(:, ones(1, Nsel));
Mt = trials(:, ones(1, Nind))';
[NewChrIx, ~] = find(Mt < Mf & [ zeros(1, Nsel); Mf(1:Nind-1, :) ] <= Mt);

NewChrom  = Chrom(NewChrIx, :);