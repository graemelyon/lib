%
% extractWeights
% Extract the correct dimension weight matrices for each layer
% Required beacuse the minFunc optimization requires a single vector
% of weights (and gradients)
%
% Graeme Lyon
% Stanford Machine Learning
% 2015
%
function [Wt1, Wt2] = extractWeights(Wts, N_layers)
N_input = N_layers(1);
N_hidden = N_layers(2);
N_output = N_layers(3);
Wt1 = reshape(Wts(1:N_hidden*(N_input+1)), N_input+1, N_hidden);
Wt2 = reshape(Wts((1+(N_hidden*(N_input+1))):end), N_hidden+1, N_output);