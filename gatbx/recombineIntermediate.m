%
% recombineIntermediate
%
% Graeme Lyon
% Jan 2015
%
% based on GA work from Sheffield Univsersity (http://codem.group.shef.ac.uk/index.php/ga-toolbox)

function NewChrom = recombineIntermediate(OldChrom)

% Snew = S1*alpha(S2-S1)

[Nind,Nvar] = size(OldChrom);

% Identify the number of matings
Xops = floor(Nind/2);

% Performs recombination
odd = 1:2:Nind-1;
even= 2:2:Nind;

% position of value of offspring compared to parents
Alpha = -0.25 + 1.5 * rand(Xops,Nvar);
NewChrom(odd,:)  = OldChrom(odd,:) + Alpha.*(OldChrom(even,:) - OldChrom(odd,:));

% the same ones more for second half of offspring
Alpha = -0.25 + 1.5 * rand(Xops,Nvar);
NewChrom(even,:) = OldChrom(odd,:) + Alpha .* (OldChrom(even,:) - OldChrom(odd,:));

% Handle case where poulaiton is odd
if rem(Nind,2),  NewChrom(Nind,:)=OldChrom(Nind,:); end

