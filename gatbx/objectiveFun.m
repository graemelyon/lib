%
% objectiveFun
%
% Graeme Lyon
% Dec 2015
%
function ObjVal = objectiveFun(Chrom, ip, target)

% train on all data or stochastically for each full pass?
deep = ip(:,1:4);
shal = ip(:,5:8);
target = target.';

for i=1:size(Chrom,1)
    
    m = Chrom(i,1:4);
    r = Chrom(i,5:8);
    c = Chrom(i,9);
    eps = Chrom(i,10);
    deltas = (deep - repmat(r,size(shal,1),1).*shal);
    
    % estimate spectral error for deltas
    [Wt1, Wt2] = extractWeights(Chrom(i,11:end), [8 16 4]);
    z2 = [ones(size(deltas,1),1) deep shal]*Wt1;
    a2 = tanh(z2);    
    z3 = [ones(size(a2,1),1) a2]*Wt2;
    err = tanh(z3);
    deltas = deltas + err;
    
    
    est = m*deltas.' + c;
    est = est.*(1 + eps*deltas(:,3) ).';
    
      
    ObjVal(i) = sqrt(sum(((target-est).^2))/length(target));
    %ObjVal(i) = sum((target-est).^2);
    
end
ObjVal = ObjVal.';