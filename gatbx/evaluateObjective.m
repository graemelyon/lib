% 
% evaluateObjective
%
% Evaluate objective of training
%
% Medtronic
% Graeme Lyon
% Jan 2015
%
function ObjVal = evaluateObjective(Chrom, ip, target)

deep = ip(:,1:4);
shal = ip(:,5:8);
target = target.';

for i=1:size(Chrom,1)
    
    m = Chrom(i,1:4);
    r = Chrom(i,5:8);
    c = Chrom(i,9);
    eps = Chrom(i,10);
    deltas = (deep - repmat(r,size(shal,1),1).*shal);
    
   
    est = m*deltas.' + c;
    est = est.*(1 + eps*deltas(:,3) ).';
      
    ObjVal(i) = sqrt(sum(((target-est).^2))/length(target));

end
ObjVal = ObjVal.';
