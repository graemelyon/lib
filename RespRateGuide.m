rates       = [8 10 15 20 25];
respPeriods = 1./(rates/60); 
duration    = 60;
restTime    = 10;

N = 100;
t = linspace(0, 2*pi - (2*pi)/N, N);

sig = cos(t);
sig = sig + 1;
sig = sig/2;
sig = -1*sig + 1;

clf; box on;
cla;
delete(h);
for i=restTime:-1:1
    h = text(0.1, 0.5, sprintf('Starting in %ds', i)); set(h, 'FontSize', 50);
    pause(1);
    delete(h);
end


for r = 1:length(respPeriods)
    
    dly = respPeriods(r)/N;

    tic;
    while toc < duration
        for i=1:length(sig)
            plot(sig(i), 'o', 'markersize', 30, 'markerfacecolor', 'b');
            ylim([0 1]);
            drawnow;
            pause(dly);
        end
    end
    
    cla;
    delete(h);
    for i=restTime:-1:1
       h = text(0.1, 0.5, sprintf('Next in %ds', i)); set(h, 'FontSize', 50);
       pause(1);
       delete(h);
    end

end

%--------------
fid = fopen('8_10_15_20_25.bin', 'r');
Rxfull = fread(fid, 'float32','ieee-le');
fclose(fid);

wt = 0.2;

t_full = 60;
fs = 48e3;
f0 = 18e3;
t  = 0:1/fs:t_full;
Tx = sin(2*pi*f0.*t);

fs2 = 16;

disp('recording...');
vr = zeros(1, round(0.1*fs)).';
y = zeros(1, 20*fs2).';
mixer = Tx(1:length(vr)).';

clf;
ax(1)=subplot(1,1,1); box on;

idx = 1:length(mixer);
tic;
while idx(end) < length(Rxfull)
    
    Rx     = Rxfull(idx);
    idx    = idx + length(mixer);
    
    nSamps = length(Rx);
    vr     = [vr(nSamps+1:end); Rx];
    mixed  = vr.*mixer;
    
    
    samp = prctile(mixed-mean(mixed),95);
    samp = wt*samp + (1-wt)*y(end); 
    y    = [y(2:end); samp];
    
    yplot     = y;
    yplot     = yplot - min(y);
    yplot     = y/max(y);
    yplot     = 1 ./ (1 + exp(-0.05*yplot));
    
    plot(ax(1), yplot, 'r');
    title(idx(end));
    pause(0.05);
    drawnow;
end


% sound(Tx, fs);
% disp('helloe');
%---------------