@echo off

set SRC=T:\data
set DEST=E:\Data\
set FILE_MATCH=*.bin *.mat ReadMe.txt

echo Copying %FILE_MATCH% files from %SRC% to %DEST%

robocopy %SRC% %DEST% %FILE_MATCH% /S /Z
